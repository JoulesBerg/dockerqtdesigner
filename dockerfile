# Use a base image with Ubuntu
FROM ubuntu:22.04

# Install necessary packages
RUN apt-get update && \
    apt-get install -y \
    qt6-tools-dev-tools \
    qt6-tools-dev \
    qt6-base-dev \
    designer-qt6 \
    xauth \
    x11-apps \
    libxcb-xinerama0 \
    libxcb-icccm4 \
    libxcb-image0 \
    libxcb-keysyms1 \
    libxcb-randr0 \
    libxcb-render-util0 \
    libxcb-shape0 \
    libxcb-shm0 \
    libxcb-sync1 \
    libxcb-xfixes0 \
    libxcb-xinerama0 \
    libxcb-xinput0 \
    libxcb-xkb1 \
    libxkbcommon-x11-0 \
    && rm -rf /var/lib/apt/lists/*

RUN echo "/usr/lib/qt6/bin\n" >> /usr/lib/x86_64-linux-gnu/qtchooser/qt6.conf && \
    echo "/usr/lib/x86_64-linux-gnu\n" >> /usr/lib/x86_64-linux-gnu/qtchooser/qt6.conf


ENV QTLIBDIR=/usr/lib/qt6
ENV PATH=$QTLIBDIR/bin:$PATH
ENV LD_LIBRARY_PATH=$QTDIR/lib:$LD_LIBRARY_PATH    
ENV QT_SELECT=qt6

# Set up environment for X11 forwarding
ENV DISPLAY=unix$DISPLAY

# Create a user to avoid running as root
RUN useradd -ms /bin/bash dev
USER dev
WORKDIR /home/dev

# Start Qt Designer
CMD ["designer"]